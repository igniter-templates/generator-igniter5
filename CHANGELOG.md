#Change Log

## 1.3.0
### Changed
* React 0.14 Upgrade, made available by `0.10.0` of `igniter-widget-library`.

### Added
* Image Carousel placeholder arrows.

## 1.2.1
### Changed
* Viewport Styling now handled in `app.js`, which handles maximizing height and width of the viewport, styling the background image, background color and body text alignment.
* `<html>` and `<body>` elements are both styled with the `viewport` style exported from `style.js`.  This ensures the styles will look the most consistent across browsers.

### Removed
* All default body styles in `_theme.scss` which are no longer needed since they're handled with inline styles now.

## 1.2.0
* Upgrade to Node v4.1.2
* Browserify template with updated jest configuration that matches igniter-widget-library.
* Updating igniter-widget-library to latest v0.9.0

## 1.1.7
### Removed
* Base style for displaying images and select elements as blocks.
* Styling for <label> elements that get in the way of inline styles we're adopting more with React.

## 1.1.6
### Changed
* `<App>` component refactored to remove dependency on jQuery for applying theme styles.
* New `style.js` module exports a style object to use for styling the `<App>` component with inline styles.
* Refactored the main run() function of the app to create a ReactRenderTarget div so that React is in full control of rendering the section and container elements.
* Updated `_package.json` to include grunt as a dev dependency.

## 1.1.5
### Added
* `formsy-react` and `jsonschema` npm dependencies required by the Lead Gen Widget.

### Changed
* Updated Bourbon styles in the browserify template to allow Igniter platform overrides for form button styles from the Lead Gen Widget.
* `App.js` renamed and updated with `<LeadGenWidget>` and new style and theme usage patterns adopted in the Lead Gen Template.
* `config.json` and `data.json` updated to include new `<LeadGenWidget>`.
* Updating igniter-widget-library bower dependency to the latest 0.8.0.

##1.1.4
###Changed
* Removed the react entry in the browser field of the template's package.json because it causes a strange bug.

##1.1.3
###Changed
* `igniter-widget-library` 0.6.0 upgrade, which includes the version bump in `bower.json` and updates to the `app.jsx` and `app-blank.jsx` files to include new usage.
* Updated Readme to point to the new location on Bitbucket for this repository.
* Upgraded `is_js` to 0.7.3

##1.1.2
###Changed
* Upgrade `igniter-widget-library` to 0.5.x, which includes `<TextWidget>` 2.0 and revamped `<SwipeImageCarousel>` plus tests.
* Example template now includes all the platform widgets available on the page.

##1.1.1
###Added
* Updated the `Gruntfile.js` to include `npm-watch`, `react-tools`, and `jquery` dev dependencies for testing.
* Watch task to the `package.json` file for running `npm run watch` to watch for changes to files during testing.

##Changed
* The src attribute of the Grunt `copy:igniterWidgetLibrary` task to exclude test files that match the globbing convention.  This is useful while developing when `igniter-widget-library` is `bower link`ed for local development.

##1.1.0
###Changed
* Upgraded to React 0.13 latest and `igniter-widget-library` gets a bump to 0.4.0, which upgrades widgets and components to React 0.13.

##1.0.15
###Changed
* Removed separate 'blank' and 'demo' templates and moved all code back into a new 'browserify' template that will allow for accommodating other types of Igniter Templates, such as the webpack version, which is still a work-in-progress but will eventually be added to the generator.
* Created `_config-blank.json`, `_data-blank.json` and `app-blank.jsx` files to use in place of the default with the `useExample` option is set to No.

###Added
* `igniterWidgetLibrary` watch task to the Gruntfile that recopies and recompiles the `app/modules/igniter-widget-library` each time the files change.  This is useful when developing in tandem with a bower linked local copy of the `igniter-widget-library` repo.

##1.0.14
###Changed
* Replaced `animateInWidgets` and associated functions with a React ref-based way to capture animateable DOM nodes with `Utilities.animateInDomNodes()` and the `getDomNodes` function, which traverses each element with a ref specified and returns each element's DOM Node as a jquery object instead of relying on widgets specified in window.igniterData, which is not a reliable source of truth of what is actually in the DOM.

###Removed
* Deprecated the `animateInWidgets` function and associated functionality from `app.jsx`.


##1.0.13
###Added
* Contributing section to the Readme.

##1.0.12
###Fixed
* Neat breakpoints `$small-screen`, `$medium-screen`, `$large-screen`, `$extra-large-screen` and all the 'up' variants are now working properly.

###Changed
* Updated Readme usage instructions to explain how to clone the Generator locally and link to the clone from npm instead of installing from npm, since the generator is a private repository still and not available through the npm registry (yet).
* `igniter-widget-library` Bower dependency locked to the latest 0.1.6.  Will have to update this each time the widget library is updated from this point on in order to get the latest version.

##1.0.11
###Removed
* fjallaone and stratumno1 fonts
* All Roboto fonts except `RobotoRegular`

##1.0.10
###Fixed
* Issue where app scripts and styles were loading in an inconsistently correct order compared to the twitter, rsvp and igniter dependencies loaded with Modernizr.load resulting in issues accessing window.igniter.analytics during local development.  Analytics calls during local development now are ensured to work since they'll be loaded in the proper order.

##1.0.9
###Added
* `igniter-widget-library` version 0.1.0 as a Bower dependency, which imports all library React components from a separate repository.

###Changed
* Updated `app.js` to `app.jsx`
* config and data.json to use new Channel Finder configuration data and new conventions
* `package.json` minor updates to the browser field.

###Removed
* All widgets, components and utilities since they now live in the `igniter-widget-library`.

##1.0.8
###Changed
* `findComponentGroupBySlug()` function now accepts an Igniter region object directly in addition to an array of Igniter componentGroup objects.

##1.0.7
###Added
* `findComponentValueBySlug()` function that retrieves a component's value directly, using the built-in `propertyExists` function to check for truthiness of the returned value.
* `getSelectedValue()` function that automatically retrieves the selected value of an Igniter select (dropdown) component.

###Changed
* `findComponentBySlug()` function now accepts an Igniter componentGroup object directly in addition to an array of Igniter component objects.

##1.0.6

###Added
* `findComponent()` utility function that allows for 1-line retrieval of a component value.

##1.0.5
###Changed
* `app/modules/widgets` directory replaces `app/modules/regions` for developing Widgets in a more logical place.
* Replaced existing 'region' components with their new 'widget' counterparts.
* Reverted a change to all hyperlinking components to open in the same window instead of a new one.

###Removed
* Channel Finder Widget since its still currently in development.  This was added by mistake.

##1.0.4
###Added
* `--skip-tests` option to the `grunt package` task that skips tests to speed up development/prototyping when necessary.

##1.0.3
###Added
* `browserify-shim` module with associated configuration changes to `package.json` and `Gruntfile.js` to allow for shimming non-CommonJS javascript modules.
* Updated the README to explain the Browserify Shim feature.
* `flowtype.js` added as a bower dependency for demonstration of the shim feature working, and will help to have it available for all templates moving forward.

###Removed
* `debowerify` is removed in favor of `browserify-shim`.

##1.0.2
###Added
* Default thumbnail with the dimensions printed on it from placehold.it to prevent broken images when uploading the default template output into the admin.

##1.0.1
###Added
* Installed grunt-shell to run the 'npm test' command from grunt since the grunt-jest plugin was wonky.
* Adding npm test command as the first in the package task

###Changed
* Moving tests out of the parent /test dir and into their component-specific directories
* Fixed the react task to ignore all '*-test.js' files

###Removed
* Removed hard browserify dependency since grunt-browserify installs its own version

##1.0.0
###Added
- Support for CommonJS Module compilation with [Browserify](http://browserify.org/).
- Support for [Jest Unit Testing](https://facebook.github.io/jest/) with `jest-cli`.  Tests for generated templates can be run with `npm test`.
- Simple starter test for the `<Text>` React component.
- `<Spinner>` component from the Youtube Carousel Template.
- This CHANGELOG.md file!
- Working unit test for this generator that asserts specific application files and directories are copied.  To run: `npm test`.

###Changed
- Refactored each javascript module in `app/templates` from using AMD to CommonJS.
- Refactor of the `<Text>` component API that allows passing children text into the component as opposed to a property.

###Removed
- Support for compiling AMD modules since (at the time of writing) are not testable with Jest.
