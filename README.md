# Igniter 5 Template Generator

###[Changelog](CHANGELOG.md)

> [Yeoman](http://yeoman.io) generator

## Getting Started

Make sure you have [Node](https://nodejs.org/download/) and [Git](http://git-scm.com/downloads) first.

### Install Yeoman

Not every new computer comes with a Yeoman pre-installed. He lives in the [npm](https://npmjs.org) package repository. You only have to ask for him once, then he packs up and moves into your hard drive. *Make sure you clean up, he likes new and shiny things.*

```
$ npm install -g yo
```

### Usage

#### Install and Run
To install `generator-igniter5`, clone the repository from BitBucket and link it with npm:

```
git clone git@bitbucket.org:igniter-templates/generator-igniter5.git
cd generator-igniter5
npm link
```

Finally, run the generator:

```
$ yo igniter5
```

The generator will scaffold out a complete template for you based on the original Theme Boilerplate.  

#### Update
To update your generator when changes are pushed to it, run:

```
cd /path/to/generator-igniter5
git pull
```
To apply the generator update to an existing template, simply change to the root directory of the template and run:
```
yo igniter5
```
Yeoman will automatically resolve file conflicts for you, prompting you to say yes or no when there are changes that will overwrite existing files.

#### Tests
There is a basic test that asserts specific primary application files are created that can be invoked by running the command:
```
npm test
```

### Features

* Custom prompt asking for the Template Name that is used to customize the following app files:

| File          | Description |
| ------------- | ------------- |
| bower.json    | Sends in a slugified version of the template name into the bower package name field, adds template name and description.  |
| config.json   | Adds the template name and description from the prompt. |
| package.json  | Does the same thing as bower.json but for package.json |
| Gruntfile.js  | Yeoman-defined 'app' and 'dist' variables are used to populate the Gruntfile to point to the correct directories. |
| README.md     | Heading uses the capitalized version of the Template Name also supplies the description and the date and version of the generator. |

* Direct copy of images/, modules/, styles/ and tests/ assets
* Allows for convenient on-the-fly template creation and updating by simply running the `yo igniter5` command with conflict resolution built-in, so there's no need to worry about overwriting files when running the generator again to pull in an update.
* Supports React JSX Compilation and boilerplate startup code.
* Browserify / CommonJS Support
* Jest Unit Testing Support

####Options
#####Demo
Scaffolds a template with sample configuration and development data and renders some basic Igniter Library components.

#####Blank
Scaffolds out a blank "starter" template with empty `config.json` and `data.json` files and renders a simple 'Fill Me In!' message to the page.

##Contributing
If you're a developer contributing to the Igniter 5 Generator then follow these steps to get setup with this repository:

###Setup
Use Git to clone this repository, then run:
```
npm install
```

###Tests
This generator supports running basic unit tests with Mocha, located in the `tests/` directory of the app.  The basic test just asserts that specific application directories and files are created, so be sure that when making changes to the files generated in the generator itself, that the test is updated to match the new directories & files that the generator outputs.

To run the tests, type:
```
npm test
```

###Versioning
We keep a [Changelog](CHANGELOG.md) that conforms to the [Semver](http://semver.org/) strategy and we use git tags to tag versions that correspond to entries in the changelog.  This is important to maintaining stability for all developers that use this generator.  

When contributing feature updates, bug fixes or any other update, make sure to do the following:
* All tests are passing
* Changelog is updated with `Added`, `Changed`, `Fixed` or `Removed` sections that outline the changes in the version.
* If the changes break anything from the previous version, uptick the minor version by 1, which is the middle value in the version tag: example: `0.1.0`
* Push changes up to a branch, open a PR and have another developer review your code before merging into master
