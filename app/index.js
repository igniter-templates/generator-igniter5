/**
 * Igniter 5 Template Generator
 * This script generates an Igniter 5 custom template.
 * Functions below run in the order they are defined.
 *
 * @author erikharper <eharper@mixpo.com>
 */
'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');
var chalk = require('chalk');
var yosay = require('yosay');
var scriptBase = require('../script-base.js')

var Igniter5Generator = yeoman.generators.Base.extend({

  /**
   * Initializing
   *
   * @return {[type]} [description]
   */
  initializing: function () {

    // Store some constants for injection in the Gruntfile
    this.app = "app";
    this.dist = "dist";

    this.pkg = require('../package.json');
  },

  /**
   * Prompting
   * @return {[type]} [description]
   */
  prompting: function () {

    // Call this.async(). This will return a function, that you then pass into your asynchronous task as a callback (see below).
    var done = this.async();

    // Igniter Greeting
    scriptBase.igniterGreeting.call(this);

    // 1. Set up Prompts
    var prompts = [
      {
        name: 'templateName',
        message: 'Provide a Template Name.  (this name will be shown on platform)',
        default: 'Template Name'
      },
      {
        name: 'templateDescription',
        message: 'Provide a Template Description.  (keep this short, maximum 30 characters)',
        default: 'Template Description'
      },
      {
        name: 'useExample',
        type: 'confirm',
        message: 'Do you want to start with an example template, which contains example configuration and demo data?',
        default: true
      }
    ];

    // 2. Handle the Prompt answers
    this.prompt(prompts, function (props) {

      this.templateName = props.templateName;
      this.templateDescription = props.templateDescription;
      this.useExample = props.useExample;

      done();
    }.bind(this));
  },

  /**
   * Writing
   * @type {Object}
   */
  writing: {

    app: function () {

      // Template Directory
      this.templateDir = 'browserify';

      // Create the app directory
      this.dest.mkdir('app');

      // Copy app-level static files
      var staticFiles = ['.gitignore', 'index.html', 'preprocessor.js'];

      // Copy Static Files
      staticFiles.forEach(function (fileName) {
        this.copy(this.templateDir + '/' + fileName, fileName);
      }, this);

      // Instead of just performing a simple copy, this.template will run Lo-Dash through the file matched from the first parameter,
      // then create and place the compiled result at the location passed in as the second argument.
      var templateFiles = [
        {
          src: '_bower.json',
          dest: 'bower.json'
        },
        {
          src: '_package.json',
          dest: 'package.json'
        },
        {
          src: '_Gruntfile.js',
          dest: 'Gruntfile.js'
        },
        {
          src: '_README.md',
          dest: 'README.md'
        },
        {
          src: 'style.js',
          dest: 'app/modules/style.js'
        }
      ];

      // Default or Blank Data?
      if (this.useExample) {
        templateFiles.push(
          {
            src: '_config.json',
            dest: 'config.json'
          },
          {
            src: '_data.json',
            dest: 'data.json'
          },
          {
            src: 'app.js',
            dest: 'app/modules/app.js'
          }
        );
      } else {
        templateFiles.push(
          {
            src: '_config-blank.json',
            dest: 'config.json'
          },
          {
            src: '_data-blank.json',
            dest: 'data.json'
          },
          {
            src: 'app-blank.js',
            dest: 'app/modules/app.js'
          }
        );
      }

      templateFiles.forEach(function (config) {
        this.template(this.templateDir + '/' + config.src, config.dest);
      }, this);
    },

    projectfiles: function () {
      var directories = ['images', 'modules', 'styles'];
      directories.forEach(function(directory, i) {
        this.directory(this.templateDir + '/' + directory, 'app/' + directory);
      }, this);
    },

    dependencies: function () {
      this.installDependencies({
        skipInstall: this.options['skip-install']
      });
    }

  },

  /**
   * End
   * @return {[type]} [description]
   */
  end: function () {
    this.spawnCommand('git', ['init']);
    this.spawnCommand('grunt', ['serve']);
  }
});

module.exports = Igniter5Generator;
