// Generated on <%= (new Date).toISOString().split('T')[0] %> using <%= pkg.name %> <%= pkg.version %>
'use strict';
var bodyParser = require('body-parser');

module.exports = function (grunt) {


  var rewriteRulesSnippet = require('grunt-connect-rewrite/lib/utils').rewriteRequest;

  /**
   * Token Replace Helper
   * Utility for token replacement.
   *
   * @param
   * @returns
   */
  var tokenReplaceHelper = function(file, token, replacement) {
    var done = this.async(),
      fs = require('fs');

      fs.readFile(file, 'utf8', function (err,data) {
      if (err) {
        done();
        console.log(err);
      }
      var result = data.replace(token, replacement);

      fs.writeFile(file, result, 'utf8', function (err) {
        done();
        if (err) {
          done();
          console.log(err);
        };
      });
    });
  };

  // show elapsed time at the end
  require('time-grunt')(grunt);

  // load all grunt tasks
  require('load-grunt-tasks')(grunt);

  // Load npm Tasks
  grunt.loadNpmTasks('grunt-browserify');

  /**
   * Grunt Configuration
   * Configures initial settings all Grunt Tasks.
   */
  grunt.initConfig({

    /**
    * Browserify Task
    * @type {Object}
    */
    browserify: {
      options: {
        transform: ['browserify-shim']
      },
      dev: {
        files: {
          'serve/assets/scripts/main.js': ['serve/modules/main.js'],
        },
        options: {
          browserifyOptions: {
            debug: true
          }
        }
      },
      dist: {
        files: {
          'serve/assets/scripts/main.js': ['serve/modules/main.js'],
        },
        options: {
          transform: ['browserify-shim', 'uglifyify']
        }
      }
    },

    /**
     * Clean Task
     * @type {Object}
     */
    clean: {
      serve: 'serve',
      dist: 'dist'
    },

    /**
     * Compress Task
     * Compresses code for distribution.
     *
     * @type {Object}
     */
    compress: {
      dist: {
        options: {
          archive: 'dist.zip'
        },
        files: [
          {
            flatten: true,
            src: ['<%= dist %>/**'],
            dest: '/'
          }
        ]
      }
    },

    /**
     * Connect Task
     * @type {Object}
     */
    connect: {
      options: {
        port: 9090,
        livereload: 35829,
        // change this to '0.0.0.0' to access the server from outside
        hostname: '*',
        base: './',
        middleware: function (connect, options) {
          var middlewares = [];

          // RewriteRules support
          middlewares.push(rewriteRulesSnippet);


          /**
           * Lead Gen Mock Endpoint
           * Returns mock data for local development for the /lead-gen ajax call.
           *
           * @link https://blog.gaya.ninja/articles/static-mockup-data-endpoints-connect/
           * @param object request  [description]
           * @param object response [description]
           * @param function next
           * @return
           */
          middlewares.unshift(function (request, response, next) {
            if(request.method == 'POST') {
              switch (request.url) {
                case '/leadgen':

                  var requestBody = request.body;
                  requestBody.id = 8;
                  requestBody.created = 1428338797;
                  requestBody.updated = 1428338797;

                  var responseBody = {
                    "code": 200,
                    "leadgen": requestBody
                  };
                  response.setHeader('content-type', 'application/json');
                  response.end(JSON.stringify(responseBody));
                  break;
                default:
                  next();
                  break;
              }
            } else {
              next();
            }
          });

          // Parse the bodies.
          middlewares.unshift(bodyParser.json());

          if (!Array.isArray(options.base)) {
            options.base = [options.base];
          }

          var directory = options.directory || options.base[options.base.length - 1];
          options.base.forEach(function (base) {
            // Serve static files.
            middlewares.push(connect.static(base));
          });

          // Make directory browse-able.
          middlewares.push(connect.directory(directory));

          return middlewares;
        },
        open: {
          target: 'http://localhost:9090' + ((grunt.option('templateUid') ? '/' + grunt.option('templateUid') + '/' : ''))
        }
      },
      livereload: {
        options: {
        }
      },
      dist: {
        options: {
          livereload: false,
          keepalive: true
        }
      },
      rules: [
        {from: (grunt.option('templateUid') ? '^/' + grunt.option('templateUid') + '(.*)' : '^/(.*)$'), to: '/serve/$1'},
        // Allow direct serving of analytics file. This is useful only when you are running the template server locally
        // for development and modification.
        {from: '^\\/global\\/([\\w]*\\.igniter\\.js)$', to: '/bower_components/igniter-page-sdk/dist/$1'}
      ]
    },

    /**
     * Copy Task
     * Copies files into directories based on environment and other conditions.
     */
    copy: {
      igniterWidgetLibrary: {
        expand: true,
        cwd: 'bower_components/igniter-widget-library/src',
        src: [
          '**',
          '!**/tests/**'
        ],
        dest: 'app/modules/igniter-widget-library'
      },
      distConfig: {
        src: 'config.json',
        dest: '<%= dist %>/config.json'
      },
      devConfig: {
        src: 'config.json',
        dest: 'serve/config.json'
      },
      distIndex: {
        src: 'serve/index.html',
        dest: '<%= dist %>/index.html'
      },
      distAssets: {
        expand: true,
        cwd: 'serve/assets',
        src: '**',
        dest: '<%= dist %>/assets'
      },
      debugConfig: {
        src: 'config.json',
        dest: 'serve/config.json'
      },
      copyFonts: {
        expand: true,
        cwd: '<%= app %>/styles/fonts',
        src: '**',
        dest: 'serve/assets/styles/fonts'
      },
      copyImages: {
        expand: true,
        cwd: '<%= app %>/images',
        src: '**',
        dest: 'serve/assets/images'
      },
      devAnalytics: {
        src: 'bower_components/igniter-page-sdk/src/analytics.js',
        dest: 'serve/assets/scripts/analytics.js'
      },
      devRSVP: {
        src: 'bower_components/rsvp/rsvp.js',
        dest: 'serve/assets/scripts/rsvp.js'
      },
      // Copies all Bower-installed CSS files as SCSS to @import them with Sass.
      vendorStyles: {
        files: [
          {
            expand: true,
            cwd: 'bower_components',
            src: ['**/*.scss', '**/*.css', '!**/*.min.css'],
            dest: 'bower_components',
            filter: 'isFile',
            ext: ".scss"
          }
        ]
      }
    },

    /**
     * Preprocess Task
     * Runs the grunt-preprocess task.
     *
     */
    preprocess: {
      indexHTMLDebug: {
        src : 'index.html', dest : 'serve/index.html',
        options: { context: { dist: false, tests: false } }
      },
      indexHTMLDist: {
        src : 'index.html', dest : 'serve/index.html',
        options: { context: { dist: true, tests: false } }
      }
    },

    /*
     * React
     * Compiles React JSX Components into javascript.
     *
     */
    react: {
      files: {
        expand: true,
        cwd: '<%= app %>/modules',
        src: ['**/*.js', '**/*.jsx', '!**/*-test.js'],
        dest: 'serve/modules',
        ext: '.js'
      }
    },

    /**
     * Inject Scripts Task
     * Placeholder for the injectScripts task
     *
     */
    injectScripts: {},

    /**
     * Sass (grunt-contrib-sass)
     * Compiles Sass Files to CSS using Ruby Sass.
     *
     * NOTE: Requires the sass-globbing gem.
     * Install it with `gem install sass-globbing`
     *
     */
    sass: {
      options: {
        loadPath: ['<%= app %>/styles', 'bower_components'],
        require: ['sass-globbing']
      },
      dist: {
        options: {
          style: 'compressed'
        },
        files: {
          'serve/assets/styles/main.css': '<%= app %>/styles/main.scss'
        }
      }
    },

   /**
    * Shell Task
    * Allows Grunt to execute any shell command.
    * In this case we're using it to execute the `npm test` command.
    * We're doing this because grunt-jest didn't work for some reason.
    * Configuration for the npm test command is in the `jest` section of package.json
    *
    * @type {Object}
    */
    shell: {
      options: {
        stderr: false
      },
      test: {
        command: 'npm test'
      }
    },

    /**
     * Watch Task
     * Handles all tasks that run while watching for changes.
     *
     * @type {Object}
     */
    watch: {
      scriptCompile: {
        files: ['<%= app %>/**/*.js', '<%= app %>/**/*.jsx'],
        tasks: ['compileScriptsDebug']
      },
      data: {
        files: ['data.json'],
        tasks: ['preprocess:indexHTMLDebug','inject']
      },
      config: {
        files: ['config.json'],
        tasks: ['copy:devConfig']
      },
      sass: {
        files: '<%= app %>/**/*.scss',
        tasks: ['sass']
      },
      livereload: {
        options: {
          livereload: 35829
        },
        files: [
          'serve/assets/**',
          'serve/assets/**',
          'serve/index.html'
        ]
      },
      igniterWidgetLibrary: {
        files: [
          'bower_components/igniter-widget-library/src/**',
          '!bower_components/igniter-widget-library/test/**',
          '!bower_components/igniter-widget-library/src/**/*-test.js'
        ],
        tasks: ['copy:igniterWidgetLibrary']
      }
    }

  });


  /**
   * Inject Scripts
   * Simulates a server render by injecting <script> tags on the served page.
   *
   * @param
   * @returns
   */
  grunt.task.registerTask('injectScripts', 'Write script tags.', function() {
    var config = grunt.file.readJSON('config.json'),
      scripts = config.scripts,
      token = /<\!--SCRIPTS-->/g,
      replacement;

    replacement = ',{load:' + JSON.stringify(scripts) + '}';

    tokenReplaceHelper.call(this, './serve/index.html', token, replacement);
  });

  /**
   * Inject Styles
   * Simulates a server render by injecting <link> tags for CSS on the served page.
   *
   * @param
   * @returns
   */
  grunt.task.registerTask('injectStyles', 'Write style tags.', function() {
    var config = grunt.file.readJSON('config.json'),
      styles = config.css,
      token = /<\!--STYLES-->/g,
      replacement;

    replacement = '{load:' + JSON.stringify(styles) + '},';

    tokenReplaceHelper.call(this, './serve/index.html', token, replacement);
  });

  /**
   * Inject Data
   * Simulates a server render by injecting mock platform data into the served page.
   *
   * @param
   * @returns
   */
  grunt.task.registerTask('injectData', 'Write mock data.', function () {
    var data = grunt.file.read('data.json'),
      token = /<\!--DATA-->/g,
      replacement;

    if (data) {
      replacement = '<script>window.igniter = {}; window.igniterData = ' + data + ';</script>';
      tokenReplaceHelper.call(this, './serve/index.html', token, replacement);
    }
  });

  /**
   * Compile Scripts Debug
   * Compiles scripts for debug mode.
   *
   * @param
   * @returns
   */
  grunt.registerTask('compileScriptsDebug', function () {
    grunt.task.run([
      'react',
      'browserify:dev'
    ]);
  });

  /**
   * Compile Scripts Dist
   * Compiles scripts for distribution mode.
   *
   * @param
   * @returns
   */
  grunt.registerTask('compileScriptsDist', function () {
    grunt.task.run([
      'react',
      'browserify:dist'
    ]);
  });

  /**
   * Inject
   * Group task that runs all the "inject" tasks.
   *
   * @param
   * @returns
   */
  grunt.registerTask('inject', function () {
    grunt.task.run([
      'injectScripts',
      'injectStyles',
      'injectData'
    ]);
  });

  /**
   * Serve
   * Starts a connect web server for development and debugging.
   *
   * @param
   * @returns
   */
  grunt.registerTask('serve', function () {
    grunt.task.run([
      'clean:serve',
      'build:debug',
      'configureRewriteRules',
      'connect:livereload',
      'watch'
    ]);
  });

  /**
   * Build Debug
   * Builds the app for debugging purposes.
   * Ran by the "serve" task.
   *
   * @param
   * @returns
   */
  grunt.registerTask('build:debug', function () {
    grunt.task.run([
      'preprocess:indexHTMLDebug',
      'inject',
      'copy:igniterWidgetLibrary',
      'compileScriptsDebug',
      'copy:copyFonts',
      'copy:copyImages',
      'copy:devConfig',
      'copy:devAnalytics',
      'copy:devRSVP',
      'copy:vendorStyles',
      'sass'
    ]);
  });

  /**
   * Build Dist
   * Builds the app for distribution.
   *
   * @param
   * @returns
   */
  grunt.registerTask('build:dist', function () {
    grunt.task.run([
      'clean:serve',
      'preprocess:indexHTMLDist',
      'inject',
      'copy:igniterWidgetLibrary',
      'compileScriptsDist',
      'copy:copyFonts',
      'copy:copyImages',
      'copy:debugConfig',
      'copy:vendorStyles',
      'sass'
    ]);
  });

  /**
   * Package
   * Packages the app for production delivery.
   *
   * Tests can be skipped by using the --skip-tests option:
   * $ grunt package --skip-tests
   *
   * @param
   * @returns
   */
  grunt.registerTask('package', function () {

    // Package Tasks
    var packageTasks = [
      'clean:dist',
      'build:dist',
      'copy:distConfig',
      'copy:distAssets',
      'copy:distIndex',
      'compress'
    ];

    // Skip Tests Option
    if (!grunt.option('skip-tests')) {
      packageTasks.unshift('shell:test');
    }

    // Run Package Tasks
    grunt.task.run(packageTasks);

  });

  /**
   * Serve Dist
   * Serves the app as if it was in production mode.
   *
   * @param
   * @returns
   */
  grunt.registerTask('serve:dist', function () {
    grunt.task.run([
      'build:dist',
      'configureRewriteRules',
      'connect:dist'
    ]);
  });

};
