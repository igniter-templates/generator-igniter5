#<%= templateName %>

<%= templateDescription %>

`Generated with Yeoman on <%= (new Date).toISOString().split('T')[0] %> using <%= pkg.name %> <%= pkg.version %>`

##Quickstart
###Prerequisites: `npm`, `bower`

```
npm install && bower install
```

###Install Dependencies

| Name          | Command                       |
| ------------- |-------------                  |
| grunt         | `npm install -g grunt-cli`    |
| sass          | `gem install sass`            |
| sass-globbing | `gem install sass-globbing`   |
| bourbon       | `gem install bourbon`         |
| bitters       | `gem install bitters`         |


## Tasks

### `serve`

The most commonly used task, running `serve` will:

* Wipe out the contents of the `serve/` directory
* Build the uncompressed application files.
    - `index.html` is preprocessed for environmental specific config
    - Reading form `config.json` script and style tags are injected
    - Page data is injected from `data.json`
    - All modules are run through the JSX transformer with `*-test.js` files ignored
    - The Browserify (via `grunt-browserify`) build tool bundles all modules together
    - Sass is compiled to CSS
    - Fonts and images are copied over to the `serve` directory
* The development server is started
* Files are watched for changes

### `package`

The `package` task will:

* Run unit tests with `jest-cli`, or skip them if the `--skip-tests` option is used.  Handy for when you want to quickly package your config.json for proof-of-concept demonstration for clients without having to wait for tests to finish.
* Build uncompressed application files.
* Optimize the build files.
* Copy the optimized files and other assets to the `dist` directory.
* Copy `config.json` to the `dist` directory.
* Copy `index.html` to the `dist` directory. (Useful for debugging, as this creates a self-contained, path-relative page bundle which can be served directly from S3. The `index.html` is not actually used by the production server).
* Create gzipped versions of everything in `dist/assets`

After running package, the template should be ready for upload directly to S3.

##Tests
Unit tests are supported with Jest and the `jest-cli` module.  Run the native jest-cli test with:
```
npm test
```
There is also a grunt task for running tests, which is bundled into the `grunt package` task:
```
grunt shell:test
```

##Features

####Browserify Shim Support for Third-Party Modules
To use third-party javascript modules (installed with bower or npm) that do not conform with the CommonJS module definition, use the Browserify Shim feature of this template by following these steps:

1. Install the module, e.g. with `bower install --save <name of module>`.
2. (Optional) Create an alias for the module by adding an entry in the `"browser"` field of `package.json` where the key represents the alias and the value represents the relative path to the javascript file.  If you choose to opt-out of creating an alias, then you will have to use the full relative path to the javascript file, relative to the location of `package.json`.
3. If the module needs a shim (i.e. it does not conform to the CommonJS module definition), then open up `app/modules/config.js` and add an entry to the exported object that defines the necessary dependencies that it needs with the `depends` property (see the comments in `app/modules/config.js` for more information).
