'use strict';

// React
var React = require('react');
var ReactDOM = require('react-dom');

// Libraries
var $ = require('jquery');
var is = require('is_js');

// Modules
var Igniter = require('./igniter-widget-library/modules/igniter');
var Utilities = require('./igniter-widget-library/modules/utilities');
var Style = require('./style');

// Components
var ImageWidget = require('./igniter-widget-library/widgets/Image/Image');
var YouTubeCarouselWidget = require('./igniter-widget-library/widgets/YouTubeCarousel/YouTubeCarousel');
var SwipeImageCarouselWidget = require('./igniter-widget-library/widgets/SwipeImageCarousel/SwipeImageCarousel');
var LeadGenWidget = require('./igniter-widget-library/widgets/LeadGen/LeadGen');
var CallToActionWidget = require('./igniter-widget-library/widgets/CallToAction/CallToAction');
var TextWidget = require('./igniter-widget-library/widgets/Text/Text');
var LinksWidget = require('./igniter-widget-library/widgets/Links/Links');
var ChannelFinderWidget = require('./igniter-widget-library/widgets/ChannelFinder/ChannelFinder');
var LoaderAccordion = require('./igniter-widget-library/components/loader-accordion/loader-accordion');

/**
 * App
 * Renders Page Regions and handles global page state and page-level animations.
 *
 */
var App = React.createClass({

  /**
   * Component Will Mount
   * @return {[type]} [description]
   */
  componentWillMount: function() {

    // Apply Viewport Styles to <html> and <body>
    Object.keys(Style.viewport).forEach(function(key) {
      document.body.style[key] = Style.viewport[key];
      document.getElementsByTagName("HTML")[0].style[key] = Style.viewport[key];
    });

  },

  /**
   * Component Did Mount
   * Fires off after this component has rendered to the DOM.
   *
   * @param
   * @returns
   */
  componentDidMount: function() {

    // Animate in all the things
    Utilities.animateInDomNodes($(ReactDOM.findDOMNode(this.refs.applicationLoader)), this.getDomNodes());

  },

  /**
   * Get Dom Nodes
   * Returns an array of jquery objects wrapping the DOM Nodes of each element that specifies a "ref" attribute.
   *
   * @param null
   * @return array
   */
  getDomNodes: function () {
    var domNodes = [];

    $.each(this.refs, function(i, ref) {
      domNodes.push($(ReactDOM.findDOMNode(ref)));
    }.bind(this));

    return domNodes;
  },

  /**
   * Render
   * Renders the Page.
   *
   * @param
   * @returns
   */
  render: function() {
    return (
      <section id='app' style={Style.section}>
        <div id='appContainer' style={Style.container}>

          <ImageWidget
            region={Igniter.findRegionBySlug('ImageWidget')}
            ref="ImageWidget" />

          <YouTubeCarouselWidget
            region={Igniter.findRegionBySlug('YouTubeCarouselWidget')}
            ref="YouTubeCarouselWidget" />

          <SwipeImageCarouselWidget
            region={Igniter.findRegionBySlug('SwipeImageCarouselWidget')}
            ref="SwipeImageCarouselWidget" />

          <LeadGenWidget
            regionSlug='lead-capture-form'
            ref='LeadCaptureForm'
            theme={Style.theme} />

          <CallToActionWidget
            region={Igniter.findRegionBySlug('CallToActionWidget')}
            ref="CallToActionWidget" />

          <TextWidget
            regionSlug="TextWidget"
            ref="TextWidget" />

          <LinksWidget
            region={Igniter.findRegionBySlug('LinksWidget')}
            ref="LinksWidget" />

          <ChannelFinderWidget
            region={Igniter.findRegionBySlug('ChannelFinderWidget')}
            ref="ChannelFinderWidget" />

          <LoaderAccordion ref="applicationLoader" />

        </div>
      </section>
    );
  }

});

module.exports = {
  run: function() {

    // Create React Render Target
    var reactRenderTarget = document.createElement("DIV");
    reactRenderTarget.id = "ReactRenderTarget";
    reactRenderTarget.setAttribute('style', 'width: 100%; height: 100%');
    document.body.insertBefore(reactRenderTarget, document.body.childNodes[0]);

    // Render the App
    ReactDOM.render(<App />, document.getElementById('ReactRenderTarget'));

  }
};
