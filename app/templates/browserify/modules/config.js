/**
 * CommonJS Module Configuration
 * Shim configuration for external javascript modules go here.
 * Browserify and browserify-shim reads this file (via package.json) to determine
 * how to compile javascript dependencies that do not support the CommonJS interface.
 *
 * Usage (paths relative to the location of package.json):
 * module.exports = {
 * 	'./bower_components/x/x.js'       :  { 'exports': '$' },
 * 	'./bower_components/x-ui/x-ui.js' :  { 'depends': { './bower_components/x/x.js': null } },
 * 	'./bower_components/y/y.js'       :  { 'exports': 'Y', 'depends': { './bower_components/x/x.js': '$' } },
 * 	'./bower_components/z/z.js'       :  { 'exports': 'zorro', 'depends': { './bower_components/x/x.js': '$', './bower_components/y/y.js': 'YNOT' } }
 * 	}
 *
 * Aliases:
 * The package.json "browser" field defines aliases for third-party modules (usually installed with Bower).
 * Those aliases can be used in this configuration in the 'depends' property.  Aliases are optional, in which
 * case you would just specify the full path to the file as shown in the Usage section above, but keep in mind
 * paths are relative to where package.json resides.
 *
 * @link https://github.com/thlorenz/browserify-shim
 * @param none
 * @returns object
 */
module.exports = {

  /**
   * FlowType
   * Depends on jquery, defined as "jQuery".
   *
   * @link http://simplefocus.com/flowtype/
   */
  'flowtype': {
    'depends': { 'jquery': 'jQuery' }
  }
}
