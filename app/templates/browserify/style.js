var is = require('is_js');
var Igniter = require('./igniter-widget-library/modules/igniter');

/**
 * Theme
 * The current theme applied to the page.
 *
 * @type {Object}
 */
var theme = {
  colors: {
    font: Igniter.findComponent('theme', 'colors', 'font'),
    background: Igniter.findComponent('theme', 'colors', 'background') || '#FFFFFF'
  },
  styles: {
    backgroundImage: Igniter.findComponent('theme', 'styles', 'background-image')
  }
};

/**
 * Viewport Style
 * Styles for the entire viewport, e.g. the <html> and <body> tags directly.
 * @type {Object}
 */
var viewport = {
  backgroundColor: theme.colors.background,
  height: '100%',
  width: '100%',
  textAlign: 'center'
};

// Background Image
if (is.truthy(theme.styles.backgroundImage)) {
  viewport.backgroundImage = 'url("' + theme.styles.backgroundImage + '")';
  viewport.backgroundRepeat = 'no-repeat';
  viewport.backgroundPosition = 'center';
  viewport.backgroundSize = 'cover';
  viewport.backgroundAttachment = 'fixed';
}

/**
 * App container style
 * Styles the container div, which frames the content.
 * @type {Object}
 */
var container = {
  width: '100%',
  maxWidth: '640px',
  margin: '0 auto'
};

/**
 * Section style
 * Styles the root <section> element, which spans the entire viewport.
 * @type {Object}
 */
var section = {
  color: theme.colors.font,
};


module.exports = {
  container: container,
  section: section,
  theme: theme,
  viewport: viewport
}
