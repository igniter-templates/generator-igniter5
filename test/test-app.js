/*global describe, beforeEach, it*/
'use strict';

var path = require('path');
var assert = require('yeoman-generator').assert;
var helpers = require('yeoman-generator').test;
var os = require('os');

describe('igniter5:app', function () {
  before(function (done) {
    helpers.run(path.join(__dirname, '../app'))
      .inDir(path.join(os.tmpdir(), './temp-test'))
      .withOptions({ 'skip-install': true })
      .withPrompt({
        someOption: true
      })
      .on('end', done);
  });

  it('creates app directories and files', function () {
    assert.file([
      'app/modules',
      'app/modules/components',
      'app/modules/widgets',
      'app/modules/app.js',
      'app/modules/style.js',
      'app/modules/main.js',
      'app/images',
      'app/images/placeholders',
      'app/styles',
      'app/styles/animations',
      'app/styles/animations/_animations.scss',
      'app/styles/animations/_keyframes.scss',
      'app/styles/bourbon',
      'app/styles/bourbon/bitters',
      'app/styles/bourbon/refills',
      'app/styles/bourbon/_bitters.scss',
      'app/styles/bourbon/_bourbon.scss',
      'app/styles/bourbon/_neat.scss',
      'app/styles/bourbon/_refills.scss',
      'app/styles/fonts',
      'app/styles/fonts/roboto',
      'app/styles/fonts/_fonts.scss',
      'app/styles/_base.scss',
      'app/styles/_theme-settings.scss',
      'app/styles/_theme.scss',
      'app/styles/main.scss',
      '.gitignore',
      'bower.json',
      'config.json',
      'Gruntfile.js',
      'package.json',
      'README.md',
      'data.json',
      'index.html',
      'preprocessor.js'
    ]);
  });
});
